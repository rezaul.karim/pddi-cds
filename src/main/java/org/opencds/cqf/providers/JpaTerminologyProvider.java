package org.opencds.cqf.providers;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jpa.dao.IFhirResourceDaoValueSet;
import ca.uhn.fhir.jpa.dao.SearchParameterMap;
import ca.uhn.fhir.jpa.provider.dstu3.BaseJpaResourceProviderValueSetDstu3;
import ca.uhn.fhir.jpa.term.IHapiTerminologySvcDstu3;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.param.UriParam;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.hl7.fhir.dstu3.model.CodeSystem;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.PlanDefinition;
import org.hl7.fhir.dstu3.model.ValueSet;
import org.hl7.fhir.dstu3.model.ValueSet.ValueSetExpansionContainsComponent;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.opencds.cqf.cdshooks.providers.Discovery;
import org.opencds.cqf.cql.runtime.Code;
import org.opencds.cqf.cql.terminology.CodeSystemInfo;
import org.opencds.cqf.cql.terminology.TerminologyProvider;
import org.opencds.cqf.cql.terminology.ValueSetInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christopher Schuler on 7/17/2017.
 */
public class JpaTerminologyProvider implements TerminologyProvider {

    private IHapiTerminologySvcDstu3 terminologySvcDstu3;
    private FhirContext context;
    private BaseJpaResourceProviderValueSetDstu3 valueSetResourceProvider;

    public JpaTerminologyProvider(IHapiTerminologySvcDstu3 terminologySvcDstu3, FhirContext context, BaseJpaResourceProviderValueSetDstu3 valueSetResourceProvider) {
        this.terminologySvcDstu3 = terminologySvcDstu3;
        this.context = context;
        this.valueSetResourceProvider = valueSetResourceProvider;
    }

    @Override
    public boolean in(Code code, ValueSetInfo valueSet) throws ResourceNotFoundException {
        for (Code c : expand(valueSet)) {
            if (c == null) continue;
            if (c.getCode().equals(code.getCode()) && c.getSystem().equals(code.getSystem())) {
                return true;
            }
        }
        return false;
    }

    private Map<String, Pair<ValueSetInfo, List<Code>> > expandedValueSetCache = new HashMap<>();

    @Override
    public Iterable<Code> expand(ValueSetInfo valueSet) throws ResourceNotFoundException {
        List<Code> codes = null;
        
        if (!expandedValueSetCache.containsKey(valueSet.getId())) {
	        codes = expandVS(valueSet);
        } 
        else {
        	Pair<ValueSetInfo, List<Code>> pair = expandedValueSetCache.get(valueSet.getId());
            
            if ((pair.getLeft().getVersion() == null && valueSet.getVersion() == null) || pair.getLeft().getVersion().equals(valueSet.getVersion())) {
                codes = pair.getRight();
            } else {
            	codes = expandVS(valueSet);
            } 
        }

        return codes;
    }
    
    public List<Code> expandVS(ValueSetInfo valueSet) throws ResourceNotFoundException {
        List<Code> codes = new ArrayList<>();
        
        IFhirResourceDaoValueSet<ValueSet, Coding, CodeableConcept> dao = (IFhirResourceDaoValueSet<ValueSet, Coding, CodeableConcept>) valueSetResourceProvider.getDao();
        boolean needsExpand = false;
        ValueSet vs;
        if (valueSet.getId().startsWith("http://") || valueSet.getId().startsWith("https://")) {
            IBundleProvider bundleProvider = valueSetResourceProvider.getDao().search(new SearchParameterMap().add(ValueSet.SP_URL, new UriParam(valueSet.getId())));
            List<IBaseResource> valueSets = bundleProvider.getResources(0, 10);
            if (!valueSets.isEmpty() && valueSets.size() == 1) {
                vs = (ValueSet) valueSets.get(0);
            }
            else if (valueSets.size() > 1) {
                throw new IllegalArgumentException("Found more than 1 ValueSet with url: " + valueSet.getId());
            }
            else {
                vs = null;
            }
        }
        else {
            vs = dao.read(new IdType(valueSet.getId()));
        }
        
        if (vs != null) {
            if (vs.hasCompose()) {
                if (vs.getCompose().hasInclude()) {
                    for (ValueSet.ConceptSetComponent include : vs.getCompose().getInclude()) {
                        if (include.hasValueSet() || include.hasFilter()) {
                            needsExpand = true;
                            ValueSet expandedValueSet = dao.expand(vs, null);
                    		for (ValueSetExpansionContainsComponent component : expandedValueSet.getExpansion().getContains()) {
                    			codes.add(new Code().withCode(component.getCode()).withSystem(component.getSystem()));
                    		}
                            break;
                        }
                        for (ValueSet.ConceptReferenceComponent concept : include.getConcept()) {
                            if (concept.hasCode()) {
                                codes.add(new Code().withCode(concept.getCode()).withSystem(include.getSystem()));
                            }
                        }
                    }
                    if (!needsExpand) {
                        return codes;
                    }
                }
            }
            
        }
        expandedValueSetCache.put(valueSet.getId(), new ImmutablePair<>(valueSet, codes));
        return codes;
    }

    @Override
    public Code lookup(Code code, CodeSystemInfo codeSystem) throws ResourceNotFoundException {
        CodeSystem cs = terminologySvcDstu3.fetchCodeSystem(context, codeSystem.getId());
        for (CodeSystem.ConceptDefinitionComponent concept : cs.getConcept()) {
            if (concept.getCode().equals(code.getCode()))
                return code.withSystem(codeSystem.getId()).withDisplay(concept.getDisplay());
        }
        return code;
    }
}