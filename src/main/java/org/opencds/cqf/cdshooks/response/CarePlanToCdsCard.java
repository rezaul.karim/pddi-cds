package org.opencds.cqf.cdshooks.response;

import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.dstu3.model.RequestGroup.RequestGroupActionComponent;
import java.util.ArrayList;
import java.util.List;

public class CarePlanToCdsCard {

    public static List<CdsCard> convert(CarePlan carePlan) {
        List<CdsCard> cards = new ArrayList<>();

        for (CarePlan.CarePlanActivityComponent activity : carePlan.getActivity()) {
            if (activity.getReferenceTarget() != null && activity.getReferenceTarget() instanceof RequestGroup) {
                RequestGroup requestGroup = (RequestGroup) activity.getReferenceTarget();
                cards = convert(requestGroup);
            }
        }

        return cards;
    }

    private static List<CdsCard> convert(RequestGroup requestGroup) {
        List<CdsCard> cards = new ArrayList<>();

        // source
        CdsCard.Source source = null;
        if (requestGroup.hasExtension()) {
            for (Extension extension : requestGroup.getExtension()) {
            	source = new CdsCard.Source();

                if (extension.getValue() instanceof Attachment) {
                    Attachment attachment = (Attachment) extension.getValue();
                    if (attachment.hasUrl()) {
                    	source.setUrl(attachment.getUrl());
                    }
                    if (attachment.hasTitle()) {
                    	source.setLabel(attachment.getTitle());
                    }
                }
                else {
                    throw new RuntimeException("Invalid source extension type: " + extension.getValue().fhirType());
                }
            }
        }

        if (requestGroup.hasAction()) {
            for (RequestGroup.RequestGroupActionComponent action : requestGroup.getAction()) {
                CdsCard card = new CdsCard();
                // basic
                if (action.hasTitle()) {
                    card.setSummary(action.getTitle());
                }
                if (action.hasDescription()) {
                    card.setDetail(action.getDescription());
                }
                if (action.hasExtension()) {
                    card.setIndicator(action.getExtensionFirstRep().getValue().toString());
                }
                
                // links
                List<CdsCard.Links> links = new ArrayList<>();
                if (action.hasDocumentation()) {
                    for (RelatedArtifact documentation : action.getDocumentation()) {
	                    CdsCard.Links link = new CdsCard.Links();
	                    
	                    if (documentation.hasDisplay()) {
	                    	link.setLabel(documentation.getDisplay());
	                    }
	                    if (documentation.hasUrl()) {
	                    	link.setUrl(documentation.getUrl());
	                    }
	
	                    links.add(link);
                    }
                }

                // suggestions
                if (action.hasAction()) {
	            	for (RequestGroupActionComponent actionComponent : action.getAction()) {
	                    boolean hasSuggestions = false;
	                    CdsCard.Suggestions suggestions = new CdsCard.Suggestions();
	                    CdsCard.Suggestions.Action actions = new CdsCard.Suggestions.Action();
	                    
	            		suggestions.setLabel(actionComponent.getLabel());
	                    hasSuggestions = true;
	                    
	                    if (actionComponent.hasType() && !actionComponent.getType().getCode().equals("fire-event")) {
	                        String code = actionComponent.getType().getCode();
	                        actions.setType(CdsCard.Suggestions.Action.ActionType.valueOf(code.equals("remove") ? "delete" : code));
	                        
	                        if (actionComponent.hasResource()) {
		                        actions.setResource(actionComponent.getResourceTarget());
		                    }

	                        if (actionComponent.hasDescription()) {
		                        actions.setDescription(actionComponent.getDescription());
		                    }
	                        
	                        suggestions.addAction(actions);
	                    }
	                    
	                    if (hasSuggestions) {
	                        card.addSuggestion(suggestions);
	                    }
	            	}
                }
                
                if (source != null) {
                    card.setSource(source);
                }
                
                if (!links.isEmpty()) {
                    card.setLinks(links);
                }
                cards.add(card);
            }
        }

        return cards;
    }
}
