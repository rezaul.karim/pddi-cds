# PDDI CDS Implementation

The PDDI CDS implementation is based on CQF Ruler which is an implementation of FHIR's [Clinical Reasoning Module](
http://hl7.org/fhir/clinicalreasoning-module.html).

## How to build, run, load

Here are the steps for starting the CQF Ruler for the PDDI CDS use case (e.g., after a fresh build and start):

1) be sure to use Java 8 
Example: $ export JAVA_HOME=/usr/lib/jvm/java-8-oracle/jre 

2) Start the server 
Example: $ mvn -Djetty.http.port=8080 jetty:run

3) Use the tag uploader to upload the resources for CDS and the two PDDI use cases

### 3.1) for CDS
Resources are located in CQF Ruler at https://git.rwth-aachen.de/binhphi109/pddi-cds/tree/master/examples/pddi-cds

1) copy codesystem-bundle.json and library-bundle.json to the folder that tag-uploader will use to upload - in this example 'pddi-cds-json'

2) use tag-uploader to load the resources to the FHIR server (in this case http://46.101.183.140:8080/baseDstu3)
$ node . -d pddi-cds-json -S http://46.101.183.140:8080/baseDstu3
-- You should see codesystem-bundle.json and library-bundle.json uploaded 

3) copy the warfarin-nsaids values sets to the folder that tag-uploader will use to upload - in this example 'pddi-cds-json'
These are the values set (located in examples/pddi-cds/warfarin-nsaids e.g., https://git.rwth-aachen.de/binhphi109/pddi-cds/tree/master/examples/pddi-cds/warfarin-nsaids)

4) use tag-uploader to load the resources to the FHIR server (in this case http://46.101.183.140:8080/baseDstu3)
$ node . -d pddi-cds-json -S http://46.101.183.140:8080/baseDstu3
-- You should see the following uploaded (do not load valueset-bundle.xml, warfarin-nsaids-cds-logic.cql, and warfarin-nsaids-cds-request.json)
activitydefinition-bundle.json
condition-bundle.json
encounter-bundle.json
library-bundle.json
medicationrequest-bundle.json
patient-bundle.json
plandefinition-bundle.json

5) Now, load the warfarin-nsaids PDDI CDS value sets using a POST of the valueset-bundle.xml (located in examples/pddi-cds/warfarin-nsaids e.g., https://git.rwth-aachen.de/binhphi109/pddi-cds/tree/master/examples/pddi-cds/warfarin-nsaids)
-- In Postman copy the XML into the body and set the body to be 'raw' 'application/xml' and then submit the POST. It should take several minutes

6) copy the digoxin-cyclosporine values sets to the folder that tag-uploader will use to upload - in this example 'pddi-cds-json'
These are the values set (located in examples/pddi-cds/warfarin-nsaids e.g., https://git.rwth-aachen.de/binhphi109/pddi-cds/tree/master/examples/pddi-cds/digoxin-cyclosporine)

7) use tag-uploader to load the resources to the FHIR server (in this case http://46.101.183.140:8080/baseDstu3)
$ node . -d pddi-cds-json -S http://46.101.183.140:8080/baseDstu3
-- You should see the following uploaded (do not load digoxin-cyclosporine-cds-request.json, digoxin-cyclosporine-cds-logic.cql)
activitydefinition-bundle.json
encounter-bundle.json
library-bundle.json
medicationrequest-bundle.json
observation-bundle.json
patient-bundle.json
plandefinition-bundle.json
valueset-bundle.xml

8) Now, load the digoxin-cyclosporine PDDI CDS value sets using a POST of the valueset-bundle.xml (located in examples/pddi-cds/digoxin-cyclosporine e.g., https://git.rwth-aachen.de/binhphi109/pddi-cds/tree/master/examples/pddi-cds/digoxin-cyclosporine)
-- In Postman copy the XML into the body and set the body to be 'raw' 'application/xml' and then submit the POST. It should take several minutes

9) Now, confirm that the value sets, library, med requests, etc are loaded to the FHIR server by querying for various named resources in the CQL file. For example, warfarin-nsaids-cds-logic.cql lists `valueset "Warfarins": 'http://hl7.org/fhir/ig/PDDI-CDS/ValueSet/valueset-warfarin'` so you should see that returned by `http://46.101.183.140:8080/baseDstu3/ValueSet?_id=valueset-warfarin`. Note that some values sets are intended for grouping such as valueset-NSAIDS. This means that you might need to search within the values sets refered to in the grouping value set. For example, `http://46.101.183.140:8080/baseDstu3/ValueSet?_id=valueset-ketorolac` is in the value set `http://hl7.org/fhir/ig/PDDI-CDS/ValueSet/valueset-NSAIDS`.

## How to test the CDS Service using HSPC Sandbox and HTTP POST 

### 3.2) Testing the CDS service from the HSPC sandbox

1) Log into https://sandbox.hspconsortium.org/

2) Go to Patients (left side of screen) and ensure that there is a patient with the  medication statement resources pasted below. Use the Patient Data Manager to edit a patient (e.g., Amy Shaw) if needed. Ensure that the RxNorm terminology is indicated (http://www.nlm.nih.gov/research/umls/rxnorm). Also, be sure that the effective date of the medication orders >= 1 day and <= 100 days from the current date:  

+ Digoxin:
```
{
  "resourceType": "MedicationStatement",
  "id": "16802",
  "meta": {
    "versionId": "2",
    "lastUpdated": "2018-10-02T14:20:35.000+00:00"
  },
  "status": "active",
  "medicationCodeableConcept": {
    "coding": [
      {
        "system": "http://www.nlm.nih.gov/research/umls/rxnorm",
        "code": "315819",
        "display": "Digoxin 0.125 MG"
      }
    ]
  },
  "effectiveDateTime": "2018-10-01",
  "dateAsserted": "2018-10-01",
  "subject": {
    "reference": "Patient/SMART-1032702"
  },
  "taken": "y",
  "isSelected": true,
  "context": {}
}
```
+ Warfarin:
```
{
  "resourceType": "MedicationStatement",
  "id": "16803",
  "meta": {
    "versionId": "2",
    "lastUpdated": "2018-10-02T14:22:55.000+00:00"
  },
  "status": "active",
  "medicationCodeableConcept": {
    "coding": [
      {
        "system": "http://www.nlm.nih.gov/research/umls/rxnorm",
        "code": "855290",
        "display": "Warfarin Sodium 1 MG Oral Tablet [Coumadin]"
      }
    ]
  },
  "effectiveDateTime": "2018-10-01",
  "dateAsserted": "2018-10-01",
  "subject": {
    "reference": "Patient/SMART-1032702"
  },
  "taken": "y",
  "isSelected": true,
  "context": {}
}
```

3) Start the CDS Sandbox from the HSPC Sandbox applications menu

4) Go to RxView and configure the CDS Service and the FHIR Server
+ CDS Service: http://46.101.183.140:8080/cds-services 
+ FHIR Server: http://46.101.183.140:8080/baseDstu3

5) To test the warfarin-NSIADS rule, create an NSAID medication request e.g., Ibuprofen 100 MG Oral Tablet. To test the digoxin-cyclosporine rule add a cylosporine order e.g., Cyclosporine 50 MG Oral Capsule. 

### 3.3) Testing the CDS service using POST requests from a command line interface or an app like Postman

1) These are canned medication-request resources in this fork of the cqf-ruler that you can post to the indicated CDS service. Use HTTP POST with the body RAW application/json to send the JSON component of these text files to the server:
- warfarin-NSAIDS: ```examples/pddi-cds/warfarin-nsaids/warfarin-nsaids-cds-request.json``` 
- digoxin-cylosporine: ```examples/pddi-cds/digoxin-cyclosporine/digoxin-cyclosporine-cds-request.json```